/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hitung_fibonacci; 

import java.math.BigInteger;
import java.util.Scanner;

/**
 *
 * @author ERIKA
 */
public class Hitung_fibonacci {

    private static void tampilJudul(String identitas) {
        System.out.println("Identitas : " + identitas);
        System.out.println("Hitung Fibonacci");
    }

    private static int tampilInput() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Bilangan ke- : ");
        int n = scanner.nextInt();

        return n;
    }

    private static BigInteger fibo(int n) {
        BigInteger[] hasil = new BigInteger[n];

        hasil[0] = BigInteger.ONE;
        hasil[1] = BigInteger.ONE;

        for (int i = 2; i < n; i++) {
            hasil[i] = hasil[i - 1].add(hasil[i - 2]);
        }
        return hasil[n - 1];
    }

    private static void tampilHasil(int n, BigInteger hasil) {
        System.out.println("Bilangan fibo ke" + n + " = " + hasil);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String identitas = "Erika Ananta / XRPL7 / 09";
        tampilJudul(identitas);
        int n = tampilInput();
        BigInteger hasil = fibo(n);
        tampilHasil(n, hasil);
    }

}
